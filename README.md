# 進擊的韭菜

### 使用語言與編譯器
Java & Eclipse

## 解題思路
每次的割韭菜行動都是獨立事件，故總共會有N * M個韭菜  
第三行開始，變數"total"為計數每行i到j的個數(j - i + 1)之總和  
最後將N * M之結果減去total(N * M - total)即為輸出之答案  

### 輸入值檢核
1. j > i
2. N值已超過int範圍，故宣告long
3. 檢查第三行以後之輸入組數是否為M

## 程式碼內容
	public static void main(String[] args) {
		// 被割韭菜之總和
		long total = 0;

		// 取得n, m
		long n = Long.parseLong(args[0]);
		int m = Integer.parseInt(args[1]);
		
		// 檢查第三行以後之輸入組數是否為m
		int countM = (args.length - 2) / 2;
		if (countM != m) {
			System.out.print("收割組數不符");
			return;
		}
		
		// 從第三行開始取值
		int tmpI = 0;
		int tmpJ = 0;
		for (int i = 2; i <= args.length - 1; i+=2) {
			tmpI = Integer.parseInt(args[i]);
			tmpJ = Integer.parseInt(args[i + 1]);
			
			// j必須大於i
			if (tmpJ <= tmpI) {
				System.out.println("j必須大於i");
				System.out.print("第" + (i - 2) + "組值輸入錯誤");
				return;
			}
			
			// 每行i到j的個數(j - i + 1)之總和
			total = total + (tmpJ - tmpI + 1);
		}
		
		// 列印結果
		long result = n * m - total;
		System.out.print(result);	
	}
